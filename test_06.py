from main_06 import create_galaxy, get_path_to_com, get_total_orbits, get_minimum_transfers


def test_get_path_to_com():
    star_map = [
         'B)C', 'C)D', 'D)E', 'E)F', 'B)G', 'G)H', 'D)I', 'E)J', 'J)K',
        'K)L', 'COM)B'
    ]
    galaxy = create_galaxy(star_map)

    assert get_path_to_com('L', galaxy) == ['L',  'K', 'J', 'E', 'D', 'C', 'B']
    assert get_path_to_com('COM', galaxy) == []
    assert get_path_to_com('D', galaxy) == ['D', 'C', 'B']


def test_get_total_orbits():
    star_map = [
        'COM)B', 'B)C', 'C)D', 'D)E', 'E)F', 'B)G', 'G)H', 'D)I', 'E)J', 'J)K',
        'K)L'
    ]
    galaxy = create_galaxy(star_map)
    assert get_total_orbits(galaxy) == 42

    star_map = [
         'B)C', 'C)D', 'D)E', 'E)F', 'B)G', 'G)H', 'D)I', 'E)J', 'J)K',
        'K)L', 'COM)B'
    ]

    galaxy = create_galaxy(star_map)
    assert get_total_orbits(galaxy) == 42


def test_orbital_transfers():
    star_map = [
        'COM)B', 'B)C', 'C)D', 'D)E', 'E)F', 'B)G', 'G)H', 'D)I', 'E)J', 'J)K',
        'K)L', 'K)YOU', 'I)SAN',
    ]
    galaxy = create_galaxy(star_map)
    assert get_minimum_transfers(galaxy) == 4
