from main_04 import get_next, get_digits

def test_get_digits():
    assert get_digits(356261) == [3, 5, 6, 2, 6, 1]

def test_get_next():
    next_val, has_pair = get_next([3,5,6,7,8,8])
    assert has_pair == True

    next_val, has_pair = get_next([1,2,3,4,4,4])
    assert has_pair == False

    next_val, has_pair = get_next([1,1,1,1,2,2])
    assert has_pair == True

    next_val, has_pair = get_next([1,1,2,2,3,3])
    assert has_pair == True

    next_val, has_pair = get_next([6,7,8,9,9,9])
    assert has_pair == False
