def get_path_to_com(code, galaxy):
    path = []

    if code == 'COM':
        return path

    while code != 'COM':
        path.append(code)
        code = galaxy[code]

    return path


def create_galaxy(star_map):
    galaxy = {}
    for codes in star_map:
        parent_code, code = [i.strip() for i in codes.split(')')]
        galaxy[code] = parent_code

    return galaxy


def get_total_orbits(galaxy):
    orbits = 0
    for code in galaxy:
        orbits += len(get_path_to_com(code, galaxy))

    return orbits


def get_minimum_transfers(galaxy):
    you_path = get_path_to_com('YOU', galaxy)
    san_path = get_path_to_com('SAN', galaxy)

    fork = [c for c in you_path if c in san_path][0]
    return (you_path.index(fork) + san_path.index(fork)) - 2


if __name__ == '__main__':
    with open('./input_06.txt', 'r') as in_file:
        star_map = in_file.readlines()

    galaxy = create_galaxy(star_map)
    print('Orbits', get_total_orbits(galaxy))
    print('Transfer', get_minimum_transfers(galaxy))
