from input_05 import INPUT
import sys

STATUS_SUSPEND = -1
STATUS_END = -2


def store_value(program, storage, value):
    if storage > len(program):
        program.extend([0 for i in range(len(program), storage + 2)])

    program[storage] = value


def get_value(program, pointer):
    if pointer > len(program):
        program.extend([0 for i in range(len(program), pointer + 2)])

    return program[pointer]


def handle_input(program, index, params, relative_base):
    storage = get_value(program, index + 1)
    store_value(program, storage, int(input('Input to {} : '.format(storage))))
    return index + 2, program


def handle_output(program, index, params, relative_base):
    pointer = parse_param(program, index + 1, params, 0, relative_base)
    print(get_value(program, pointer))
    return index + 2, program


def handle_jump_true(program, index, params, relative_base):
    pointer_a = parse_param(program, index + 1, params, 0, relative_base)
    if get_value(program, pointer_a) != 0:
        pointer_b = parse_param(program, index + 2, params, 1, relative_base)
        return get_value(program, pointer_b), program

    return index + 3, program


def handle_jump_false(program, index, params, relative_base):
    pointer_a = parse_param(program, index + 1, params, 0, relative_base)

    if get_value(program, pointer_a) == 0:
        pointer_b = parse_param(program, index + 2, params, 1, relative_base)
        return get_value(program, pointer_b), program

    return index + 3, program


def handle_less_than(program, index, params, relative_base):
    pointer_a = parse_param(program, index + 1, params, 0, relative_base)
    pointer_b = parse_param(program, index + 2, params, 1, relative_base)
    storage = parse_param(program, index + 3, params, 2, relative_base)

    if get_value(program, pointer_a) < get_value(program, pointer_b):
        store_value(program, storage, 1)
    else:
        store_value(program, storage, 0)

    return index + 4, program


def handle_equal(program, index, params, relative_base):
    pointer_a = parse_param(program, index + 1, params, 0, relative_base)
    pointer_b = parse_param(program, index + 2, params, 1, relative_base)
    storage = parse_param(program, index + 3, params, 2, relative_base)

    if get_value(program, pointer_a) == get_value(program, pointer_b):
        store_value(program, storage, 1)
    else:
        store_value(program, storage, 0)

    return index + 4, program


def parse_param(program, index, params, offset, relative_base=0):
    if len(params) >= offset + 1:
        # Immediate Mode
        if params[offset] == 1:
            return index

        # Relative Mode
        if params[offset] == 2:
            return get_value(program, index) + relative_base

    # Position Mode
    return get_value(program, index)


def handle_math(func):
    def _handler(program, index, params, relative_base):
        index_a = parse_param(program, index + 1, params, 0, relative_base)
        index_b = parse_param(program, index + 2, params, 1, relative_base)
        storage = parse_param(program, index + 3, params, 2, relative_base)
        store_value(
            program,
            storage,
            func(get_value(program, index_a), get_value(program, index_b))
        )

        return index + 4, program
    return _handler


def parse_modes(code):
    str_code = str(code)
    op_code = int(str_code[-2:])
    params = reversed([int(i) for i in str_code[:-2]])
    return op_code, list(params)


def get_relative_base(program, index, params, relative_base):
    pointer = parse_param(program, index + 1, params, 0, relative_base)
    return index + 2, get_value(program, pointer)


OPS = {
    1: handle_math(lambda x, y: x + y),
    2: handle_math(lambda x, y: x * y),
    3: handle_input,
    4: handle_output,
    5: handle_jump_true,
    6: handle_jump_false,
    7: handle_less_than,
    8: handle_equal,
}

def process(program, index=0, ops=OPS):
    relative_base = 0

    while index < len(program):
        code = program[index]

        if code == 99:
            return program, STATUS_END

        op_code, params = parse_modes(code)

        if op_code == 9:
            new_index, relative_base = get_relative_base(program, index, params, relative_base)
        else:
            op = ops.get(op_code)
            new_index, program = op(program, index, params, relative_base)

        if new_index == STATUS_SUSPEND:
            return program, STATUS_SUSPEND
        else:
            index = new_index

    return program, index


if __name__ == '__main__':
    initial = [int(i) for i in INPUT.split(',')]
    process(initial)
