import sys
from itertools import permutations
from main_05 import process, OPS, parse_param, STATUS_SUSPEND, STATUS_END


def _prepend_last(sequence, value):
    last = len(sequence) - 1
    if last > -1:
        sequence.append(sequence[last])
        sequence[last] = value


def run_amplifier(amp):
    outputs = []
    results = []

    def handle_input(program, index, params=None):
        pointer = program[index + 1]

        try:
            program[pointer] = amp['input_signals'].pop()
        except IndexError:
            amp['index'] = index
            return STATUS_SUSPEND, program

        return index + 2, program

    def handle_output(program, index, params=None):
        pointer = parse_param(program, index + 1, params, 0)

        amp['output_signals'].insert(0, program[pointer])
        amp['result'] = program[pointer]

        return index + 2, program

    ops = dict(OPS)
    ops[3] = handle_input
    ops[4] = handle_output

    amp['program'], amp['status'] = process(amp['program'], amp['index'], ops)


def runner(initial_program, sequence):
    buffers = []
    result = None
    LETTERS = 'ABCDE'

    amps = []
    for signal in sequence:
        amps.append({
            'input_signals': [int(signal)],
            'output_signals': None,
            'program': list(initial_program),
            'status': None,
            'index': 0,
        })
    amps[0]['input_signals'].insert(0, 0)
    amps_len = len(amps)

    for idx, amp in enumerate(amps):
        amp['output_signals'] = amps[(idx + 1) % amps_len]['input_signals']

    is_complete = any([a['status'] == STATUS_END for a in amps])
    amp_index = 0

    while not is_complete:
        amp = amps[amp_index]
        result = run_amplifier(amp)
        amp_index = (amp_index + 1) % amps_len

        is_complete = all([a['status'] == STATUS_END for a in amps])

    return amps[4]['result']


if __name__ == '__main__':
    program = [
        3,8,1001,8,10,8,105,1,0,0,21,30,55,76,97,114,195,276,357,438,99999,3,9,102,3,9,9,4,9,99,3,9,1002,9,3,9,1001,9,5,9,1002,9,2,9,1001,9,2,9,102,2,9,9,4,9,99,3,9,1002,9,5,9,1001,9,2,9,102,5,9,9,1001,9,4,9,4,9,99,3,9,1001,9,4,9,102,5,9,9,101,4,9,9,1002,9,4,9,4,9,99,3,9,101,2,9,9,102,4,9,9,1001,9,5,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,99,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,99
    ]
    outputs = []
    sequences = permutations([9,8,7,6,5])
    for sequence in sequences:
        outputs.append(runner(program, sequence))

    print(max(outputs))
