from main_08 import get_layers, get_pixels


def test_get_layers():
    data = [int(i) for i in '123456789012']
    assert get_layers(data, 3, 2) == [
        [1,2,3,4,5,6],
        [7,8,9,0,1,2]
    ]


def test_get_pixels():
    data = [int(i) for i in '0222112222120000']
    layers = get_layers(data, 2, 2)

    assert get_pixels(layers, 2, 2) == [
        0, 1, 1, 0,
    ]
