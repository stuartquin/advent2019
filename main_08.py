BLACK = 0
WHITE = 1


COLORS = {
    WHITE: '#',
    BLACK: ' '
}


def chunks(data, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(data), n):
        yield data[i:i + n]


def get_layers(data, width, height):
    return list(chunks(data, width * height))


def get_checksum(layers):
    min_zero = None
    min_layer = 0
    for layer in layers:
        count = layer.count(0)

        if min_zero is None or min_zero > count:
            min_zero = count
            min_layer = layer

    return min_layer.count(1) * min_layer.count(2)


def get_pixel(layers, index):
    for layer in layers:
        if layer[index] < 2:
            return layer[index]

    return 2

def get_pixels(layers, width, height):
    return [get_pixel(layers, i) for i in range(0, width * height)]


def runner():
    width = 25
    height = 6

    with open('./input_08.txt', 'r') as in_file:
        data = [int(i) for i in in_file.read().strip()]

    print(data)

    layers = get_layers(data, width, height)
    pixels = [COLORS.get(p) for p in get_pixels(layers, width, height)]

    for line in chunks(pixels, 25):
        print(''.join(line))


if __name__ == '__main__':
    print(runner())
