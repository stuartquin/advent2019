from main_03 import process, count_steps


def test_simple():
    res = process('R8,U5,L5,D3', 'U7,R6,D4,L4')
    assert res == 6

def test_one():
    res = process('R75,D30,R83,U83,L12,D49,R71,U7,L72', 'U62,R66,U55,R34,D71,R55,D58,R83')
    assert res == 159

def test_two():
    res = process('R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51', 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7')
    assert res == 135

def test_count_steps_one():
    res = count_steps(
        'R75,D30,R83,U83,L12,D49,R71,U7,L72',
        'U62,R66,U55,R34,D71,R55,D58,R83'
    )
    assert res == 610

def test_count_steps_two():
    res = count_steps(
        'R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51',
        'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7',
    )
    assert res == 410

