import math
DIVISORS = [10, 100, 1000, 10000, 100000, 1000000]
DIVISORS = [1000000, 100000, 10000, 1000, 100, 10]

def get_digits(value):
    res = [value % i for i in DIVISORS]
    result = []
    for idx, i in enumerate(res):
        if idx < len(DIVISORS) - 1:
            result.append(round(math.floor(res[idx] / DIVISORS[idx + 1])))
        else:
            result.append(res[idx])

    return result


def get_next(digits):
    """
    Get next incremental value such that no position is decremented
    """
    next_val = [digits[0]]
    range_lens = []
    range_len = 1

    for idx in range(1, 6):
        digit = digits[idx]
        if digit <= digits[idx - 1]:
            next_val.append(next_val[idx - 1])
        else:
            next_val.append(digit)

        if next_val[idx] == next_val[idx - 1]:
            range_len += 1
        else:
            range_lens.append(range_len)
            range_len = 1

    range_lens.append(range_len)
    return next_val, 2 in range_lens


def to_int(digits):
    return int(''.join([str(x) for x in digits]))


def crack_password(start, end):
    count = 0
    current = start
    next_val = get_digits(current)
    while current < end:
        next_val, has_pair = get_next(get_digits(current))
        current = to_int(next_val)

        if has_pair:
            count += 1
            print('INC', current, count)

        current += 1

    print(count - 1)

if __name__ == '__main__':
    crack_password(356261, 846303)
