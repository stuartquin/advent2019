from main_05 import parse_modes, process

def _assert_equal(x, y):
    assert x == y

def _assert_len(x, y):
    assert len(x) == y


def test_parse_modes():
    op_code, params = parse_modes('1002')
    assert op_code == 2
    assert params == [0, 1]

    op_code, params = parse_modes('3')
    assert op_code == 3
    assert params == []


def test_simple_add():
    program, index = process([1, 0,  0, 0, 99])
    assert program == [2, 0, 0, 0, 99]


def test_simple_mult():
    program, index = process([2,3,0,3,99])
    assert program == [2,3,0,6,99 ]


def test_equal_eight(monkeypatch):
    """ If input == 8, output 1, else output 0"""
    initial = [3,9,8,9,10,9,4,9,99,-1,8]

    monkeypatch.setattr('builtins.input', lambda x: '8')
    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 1))
    process(initial)

    monkeypatch.setattr('builtins.input', lambda x: '9')
    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 0))
    process(initial)


def test_less_than_eight(monkeypatch):
    initial = [3,9,7,9,10,9,4,9,99,-1,8]

    monkeypatch.setattr('builtins.input', lambda x: '5')
    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 1))
    process(initial)

    monkeypatch.setattr('builtins.input', lambda x: '9')
    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 0))
    process(initial)


def test_immediate_equal_eight(monkeypatch):
    initial = [3,3,1108,-1,8,3,4,3,99]

    monkeypatch.setattr('builtins.input', lambda x: '8')
    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 1))
    process(initial)

    monkeypatch.setattr('builtins.input', lambda x: '9')
    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 0))
    process(initial)


def test_immediate_less_than_eight(monkeypatch):
    initial = [3,3,1107,-1,8,3,4,3,99]

    monkeypatch.setattr('builtins.input', lambda x: '5')
    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 1))
    process(initial)

    monkeypatch.setattr('builtins.input', lambda x: '9')
    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 0))
    process(initial)


def test_positon_mode(monkeypatch):
    initial = [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9]
    monkeypatch.setattr('builtins.input', lambda x: '5')
    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 1))
    process(initial)


def test_immediate_mode(monkeypatch):
    initial = [3,3,1105,-1,9,1101,0,0,12,4,12,99,1]
    monkeypatch.setattr('builtins.input', lambda x: '0')
    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 0))
    process(initial)


def test_larger(monkeypatch):
    initial = [
        3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
        1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
        999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99
    ]

    monkeypatch.setattr('builtins.input', lambda x: '7')
    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 999))
    process(initial)

    monkeypatch.setattr('builtins.input', lambda x: '8')
    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 1000))
    process(initial)

    monkeypatch.setattr('builtins.input', lambda x: '9')
    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 1001))
    process(initial)


def test_relative_base(monkeypatch):
    initial = [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99]
    program, status = process(initial)
    assert program == initial

    monkeypatch.setattr('builtins.print', lambda x: _assert_len(str(x), 16))
    initial = [1102,34915192,34915192,7,4,7,99,0]
    program, status = process(initial)

    monkeypatch.setattr('builtins.print', lambda x: _assert_equal(x, 1125899906842624))
    initial = [104,1125899906842624,99]
    program, status = process(initial)
